/*
 Navicat Premium Data Transfer

 Source Server         : PHPMYADMIN
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : cms

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 08/12/2018 15:27:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bukutamu
-- ----------------------------
DROP TABLE IF EXISTS `bukutamu`;
CREATE TABLE `bukutamu`  (
  `bukutamu_id` int(11) NOT NULL AUTO_INCREMENT,
  `bukutamu_idkegiatan` int(11) NULL DEFAULT NULL,
  `bukutamu_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bukutamu_email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bukutamu_notlp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bukutamu_alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `bukutamu_instansi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `bukutamu_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`bukutamu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bukutamu
-- ----------------------------
INSERT INTO `bukutamu` VALUES (1, 77, 'duwi', 'haryanto.duwi@gmail.com', '085725818424', 'pedak wijirejo pandak bantul', 'SMA N 1 Bantul', '2018-11-30');
INSERT INTO `bukutamu` VALUES (3, 81, 'bayu', 'bayu@gmail.com', '085725818424', 'bantul', 'bantul', '2018-11-30');
INSERT INTO `bukutamu` VALUES (4, 77, 'bayu', 'bayu@gmail.com', '085725818424', 'bantul', 'bantul', '2018-11-30');
INSERT INTO `bukutamu` VALUES (5, 77, 'vera', 'vera@gmail.com', '085725818424', 'bantul', 'bantul', '2018-11-30');

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori`  (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kategori_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`kategori_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES (1, 'berita', '2018-12-02');
INSERT INTO `kategori` VALUES (2, 'pengumuman', '2018-12-02');
INSERT INTO `kategori` VALUES (10, 'agenda', '2018-12-02');
INSERT INTO `kategori` VALUES (11, 'kontak', '2018-12-02');
INSERT INTO `kategori` VALUES (12, 'download', '2018-12-02');

-- ----------------------------
-- Table structure for kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE `kegiatan`  (
  `kegiatan_id` int(255) NOT NULL AUTO_INCREMENT,
  `kegiatan_nama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_date` date NULL DEFAULT NULL,
  `kegiatan_file` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_aktif` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`kegiatan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kegiatan
-- ----------------------------
INSERT INTO `kegiatan` VALUES (77, 'Pameran/Roadshow', 'Pameran di SMA N 1 Bantul', '2018-11-30', NULL, b'1');
INSERT INTO `kegiatan` VALUES (78, 'Pameran/Roadshow', 'Roadshow di SMA N 3 bantul', '2018-11-30', '8a29bd0fe8520378a3238de644f2d8f7.pdf', b'0');
INSERT INTO `kegiatan` VALUES (81, 'Pameran/Roadshow', 'Roadshow di SMA 2 N bantul', '2018-12-01', NULL, b'0');

-- ----------------------------
-- Table structure for keunggulan
-- ----------------------------
DROP TABLE IF EXISTS `keunggulan`;
CREATE TABLE `keunggulan`  (
  `keunggulan_id` int(11) NOT NULL AUTO_INCREMENT,
  `keunggulan_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keunggulan_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keunggulan_ikon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keunggulan_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`keunggulan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of keunggulan
-- ----------------------------
INSERT INTO `keunggulan` VALUES (1, 'dwad', 'dawd', 'fa fa-gears', '2018-12-08');
INSERT INTO `keunggulan` VALUES (2, 'Codeigniter', 'aaaaaaaaaaaaaaaaaaa', 'fa fa-gears', '2018-12-08');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_ikon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_is_mainmenu` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_akses_level` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_urutan` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_status` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_akses` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (4, 'home', 'fa fa-tasks', '0', 'frontend/home', '0', '1', '0', 0);
INSERT INTO `menu` VALUES (5, 'download', 'fa fa-download', '0', 'frontend/download', '0', '3', '0', 0);
INSERT INTO `menu` VALUES (6, 'kegiatan', 'fa fa-tasks', '0', 'frontend/kegiatan', '0', '2', '0', 0);
INSERT INTO `menu` VALUES (7, 'kegiatan', 'fa  fa-calendar-check-o ', '0', 'kegiatan/admin', '1', '1', '0', 1);
INSERT INTO `menu` VALUES (8, 'bukutamu', 'fa fa-book', '0', 'bukutamu/admin', '1', '1', '0', 1);
INSERT INTO `menu` VALUES (9, 'user', 'fa fa-circle-o', '20', 'user/admin', '1', '99', '1', 1);
INSERT INTO `menu` VALUES (10, 'bukutamu', 'fa fa-book', '0', 'frontend/bukutamu', '0', '1', '1', 0);
INSERT INTO `menu` VALUES (11, 'login', 'fa fa-lock', '0', 'login', '0', '2', '1', 0);
INSERT INTO `menu` VALUES (14, 'berita', 'fa fa-newspaper-o', '0', 'kategori/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (15, 'kategori', 'fa fa-circle-o', '14', 'kategori/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (16, 'post', 'fa fa-circle-o', '14', 'post/admin', '1', '2', '1', 1);
INSERT INTO `menu` VALUES (17, 'testimoni', 'fa fa-users', '0', 'testimoni/admin', '1', '3', '1', 1);
INSERT INTO `menu` VALUES (18, 'tools', 'fa fa-tasks', '0', 'tools/admin', '1', '4', '1', 1);
INSERT INTO `menu` VALUES (19, 'keunggulan', 'fa fa-tasks', '0', 'keunggulan/admin', '1', '5', '1', 1);
INSERT INTO `menu` VALUES (20, 'setting', 'fa fa-gears', '0', 'setting/admin', '1', '6', '1', 1);
INSERT INTO `menu` VALUES (21, 'tentang', 'fa fa-circle-o', '20', 'tentang/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (22, 'sosial media', 'fa fa-circle-o', '20', 'sosialmedia/admin', '1', '2', '1', 1);

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `post_konten` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `post_date` date NULL DEFAULT NULL,
  `post_idkategori` int(255) NULL DEFAULT NULL,
  `post_featuredimage` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `post_iduser` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES (3, 'Jadwal Ujian Tahap satu', 'hello wolrdddwadaw', '2018-12-02', 10, NULL, '1');
INSERT INTO `post` VALUES (4, 'Pengumuman lolos seleksi administrasi', 'dawdaw', '2018-12-02', 10, '2e7a884efa803b106baba648905dd4b0.jpeg', '1');
INSERT INTO `post` VALUES (5, 'pengumuman ujian', 'dwadawdawd', '2018-12-02', 1, NULL, '1');

-- ----------------------------
-- Table structure for tentang
-- ----------------------------
DROP TABLE IF EXISTS `tentang`;
CREATE TABLE `tentang`  (
  `tentang_id` int(11) NOT NULL AUTO_INCREMENT,
  `tentang_judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tentang_konten` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`tentang_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for testimoni
-- ----------------------------
DROP TABLE IF EXISTS `testimoni`;
CREATE TABLE `testimoni`  (
  `testimoni_id` int(11) NOT NULL AUTO_INCREMENT,
  `testimoni_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `testimoni_pekerjaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `testimoni_konten` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `testimoni_foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `testimoni_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`testimoni_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of testimoni
-- ----------------------------
INSERT INTO `testimoni` VALUES (1, 'duwi haryanto', 'backend programer', 'selama menggunakan jasa pembuatan website tidak pernah mengecewakan selalu memberikan yang terbaik', NULL, '2018-12-03');
INSERT INTO `testimoni` VALUES (2, 'ariana', 'web programer', 'Selalu memberikan pelayanan primaaaa', '9c2c88ceea6b422b2b4bc357221736a8.jpeg', '2018-12-03');
INSERT INTO `testimoni` VALUES (3, 'jhon doe', 'web analysis', 'saya sangat bersemangat ketika berkerja sama dengan perusahaan ini, tidak pernah mengecewakan', 'cc432a1e416db679e8d366ea68d5282d.jpeg', '2018-12-03');

-- ----------------------------
-- Table structure for tools
-- ----------------------------
DROP TABLE IF EXISTS `tools`;
CREATE TABLE `tools`  (
  `tools_id` int(11) NOT NULL AUTO_INCREMENT,
  `tools_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tools_presentase` int(255) NULL DEFAULT NULL,
  `tools_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`tools_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tools
-- ----------------------------
INSERT INTO `tools` VALUES (1, 'photoshop', 90, '2018-12-03');
INSERT INTO `tools` VALUES (2, 'PHP', 80, '2018-12-03');
INSERT INTO `tools` VALUES (3, 'bootstrap', 90, '2018-12-03');
INSERT INTO `tools` VALUES (4, 'html', 80, '2018-12-03');
INSERT INTO `tools` VALUES (6, 'adobe ilustrator', 85, '2018-12-03');
INSERT INTO `tools` VALUES (7, 'javascript', 75, '2018-12-03');
INSERT INTO `tools` VALUES (8, 'ajax', 70, '2018-12-03');
INSERT INTO `tools` VALUES (9, 'codeigniter', 95, '2018-12-03');
INSERT INTO `tools` VALUES (10, 'CSS 3', 75, '2018-12-03');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_level` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_terdaftar` date NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 'administrator', '1', '2018-09-29');
INSERT INTO `user` VALUES (3, 'haryanto', 'haryanto', 'haryanto duwi', '2', '2018-10-21');
INSERT INTO `user` VALUES (4, 'duwi', 'duwi', 'duwi', '1', '2018-11-30');

SET FOREIGN_KEY_CHECKS = 1;
