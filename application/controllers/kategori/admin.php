<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		// if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
		// 	redirect(site_url('login/logout'));
		// }
		$this->cekadmin();
	}
	//VARIABEL
	private $master_tabel="kategori"; //Mendefinisikan Nama Tabel
	private $id="kategori_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="kategori/admin/"; //Mendefinisikan url controller
	private $default_view="kategori/admin/"; //Mendefinisiakn defaul view
	private $view="template/backend"; //Mendefinisikan Tamplate Root
	private $path='./upload/';

	private function global_set($data){
		$data=array(
			'menu'=>'berita',//Seting menu yang aktif
			'menu_submenu'=>false,
			'submenu_menu'=>'kategori',
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa fa-tasks",
			'view'=>"views/kategori/admin/index.php",
			'detail'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>true,
			'download'=>false,
			'aktifasi'=>false,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=kategori, bentuk obyek
		//$data['menu']=kategori, array bentuk biasa
	}
	private function hapus_file($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->id=>$id)),
		);
		$file=$this->Crud->read($query)->row();
		if($file->kategori_file){
			unlink($this->path.$file->kategori_file);
		}
		//return $file->kategori_aktif;
	}
	public function index()
	{
		$global_set=array(
			'headline'=>'kategori',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		//CEK SUBMIT DATA
		if($this->input->post('kategori_nama')){
			//PROSES SIMPAN
			$data=array(
				'kategori_nama'=>$this->input->post('kategori_nama'),
				'kategori_date'=>date('Y-m-d',strtotime($this->input->post('kategori_date'))),
			);
			#############################################
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['kategori_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$dt['error']=$this->upload->display_errors();
			// 		return $this->output->set_output(json_encode($dt));
			// 		exit();
			// 	}
			// }			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			if($insert){
				$dt['success']='input data berhasil';
			}else{
				$dt['error']='input data error';
			}
			return $this->output->set_output(json_encode($dt));
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu_backend($this->session->userdata('user_level')),
			);
			//$this->dumpdata($data);			
			//$this->load->view($this->view,$data);
			$this->backend($data);
		}
	}
	public function tabel(){
		$global_set=array(
			'headline'=>'kategori',
			'url'=>$this->default_url,
		);
		//LOAD FUNCTION GLOBAL SET
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>$this->id,'orderby'=>'DESC'),
		);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function edit(){
		$global_set=array(
			'headline'=>'edit data',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('kategori_nama')){
			//PROSES SIMPAN
			$data=array(
				'kategori_nama'=>$this->input->post('kategori_nama'),
				'kategori_date'=>date('Y-m-d',strtotime($this->input->post('kategori_date'))),
			);
			###################################################
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$this->hapus_file($id);
			// 		$file=$this->upload->data('file_name');
			// 		$data['kategori_file']=$file;
			// 	}else{
			// 		$dt['error']=$this->upload->display_errors();
			// 		return $this->output->set_output(json_encode($dt));
			// 		exit();	
			// 	}
			// }			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array($this->id=>$id),
			);
			$update=$this->Crud->update($query);
			if($update){
				$dt['success']='update data berhasil';
				$dt['id']=$id;
			}else{
				$dt['error']='input data error';
			}
			return $this->output->set_output(json_encode($dt));			
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->id=>$id))
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->viewdata($data);			
			$this->load->view($this->default_view.'edit',$data);
		}			
	}	
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'add data',
			'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
		);	
		$global=$this->global_set($global_set);
		$data=array(
			'global'=>$global,
			);
		$this->load->view($this->default_view.'add',$data);		
	}	
	public function hapus(){
		$id=$this->input->post('id');
		//$kategori=$this->hapus_file($id);
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->id=>$id),
		);
		$delete=$this->Crud->delete($query);
		if($delete){
			$dt['success']='hapus data berhasil';
		}else{
			$dt['error']='input data error';
			$dt['msg']=$delete;
		}
		return $this->output->set_output(json_encode($dt));	
	}
	public function download($file=null){
		if($file){
			$this->downloadfile($this->path,$file);
		}else{
			$this->session->set_flashdata('error','File tidak ditemukan');
			redirect(site_url($this->default_url));
		}
	}
	public function aktifasi(){
		############### ALL SET 0
		$prepare=array(
			'kategori_aktif'=>0,
		);
		$qprepare=array(
			'tabel'=>$this->master_tabel,
			'data'=>$prepare,
		);
		$prepare=$this->Crud->update($qprepare);
		############### SET KE AKTIF
		$id=$this->input->post('id');
		$data=array(
			'kategori_aktif'=>1,
		);
		$query=array(
			'tabel'=>$this->master_tabel,
			'data'=>$data,
			'where'=>array($this->id=>$id),
		);
		$update=$this->Crud->update($query);
		if($update){
			$dt['success']='Aktifasi sukses';
		}else{
			$dt['error']='Aktifasi gagal';
		}
		return $this->output->set_output(json_encode($dt));

	}

}
