<div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-yellow-active">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><?= ucwords($global->headline)?></h4>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url.'edit')?>"  enctype="multipart/form-data">			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
					<div class="form-group ">
							<label>Id</label>
							<input readonly type="text" name="id" class="form-control" title="Harus di isi" value="<?= $data->tools_id?>">
						</div>					
						<div class="form-group">
							<label>Tanggal</label>
							<input type="text" readonly name="tools_date" class="form-control" value="<?= date('d-m-Y')?>">
						</div>							
						<div class="form-group">
							<label>Nama Tools</label>
							<input required type="text" name="tools_nama" class="text-capitalize form-control" title="Harus di isi" value="<?=$data->tools_nama?>">
							<p class="help-block">Photoshop, Ilustration</p>
						</div>
						<div class="form-group">
							<label>Presentase</label>
							<input required type="text" name="tools_presentase" class="text-capitalize form-control" title="Harus di isi" max="100" min="1" value="<?=$data->tools_presentase?>">
							<p class="help-block">Angka 1 - 100</p>
						</div>																													 
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">						
						<div class="form-group">
							<button type="submit" value="submit" name="submit" class="btn btn-warning btn-block btn-flat">Update</button>
						</div>														
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</div>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');	
</script>
<?php include 'action.php'?>