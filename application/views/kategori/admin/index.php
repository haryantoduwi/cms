<div class="row">
	<div class="col-sm-12">
		<h4 style="border-left: 5px solid #245571; padding-left: 10px">Kategori berita</h4>
	</div>
	<div class="col-sm-2">
		<button id="add" url="<?= base_url($global->url.'add')?>" onclick="add()" class="btn btn-flat btn-block btn-primary" ><i class="fa fa-plus"></i> Tambah Data</button>
	</div>
</div>
<br>
<div id="view">
	<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
		<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
	</div>
</div>
<div id="modal"></div>
<script type="text/javascript">
	$(document).ready(function(){
		var url=$('#tabel').attr('url');
		    setTimeout(function () {
	        $("#tabel").load(url);
	    }, 200); 		
	})
</script>
<?php include 'action.php';?>